package com.ion.ld25.astar;

import java.util.ArrayList;

import com.ion.ld25.entities.Building;
import com.ion.ld25.map.EntityMap;

public class Astar {
	
	public static ArrayList<Node> open = new ArrayList<Node>();
	public static ArrayList<Node> closed = new ArrayList<Node>();
	public static boolean done = false;
	public static int gx;
	public static int gy;
	public static int ci;
	
	public static ArrayList<Integer> path(int x1, int y1, int x2, int y2){
		open.clear();
		closed.clear();
		done = false;
		gx = x2;
		gy = y2;
		open.add(new Node(x1, y1, x2, y2, null));
		while(!done){
			ci = 0;
			double f = open.get(ci).f;
			for(int n = 0; n < open.size(); n ++){
				if(open.get(n).f < f){
					ci = n;
				}
			}
			closed.add(open.get(ci));
			if(closed.get(closed.size() - 1).x == gx && closed.get(closed.size() - 1).y == gy){
				done = true;
			}
			open.remove(ci);
			ci = closed.size() - 1;
			check(closed.get(ci).x + 1, closed.get(ci).y);
			check(closed.get(ci).x - 1, closed.get(ci).y);
			check(closed.get(ci).x, closed.get(ci).y + 1);
			check(closed.get(ci).x, closed.get(ci).y - 1);
		}
		return result();
	}
	
	public static void check(int x, int y){
		boolean go = true;
		for(int n = 0; n < closed.size(); n++){
			if(closed.get(n).x == x && closed.get(n).y == y){
				go = false;
			}
		}
		if(go){
			for(int n = 0; n < EntityMap.ents.size(); n++){
				if(x == EntityMap.ents.get(n).xco && y == EntityMap.ents.get(n).yco){
					go = false;
					if(x == gx && y == gy){
						go = true;
					}
				}
				else if(EntityMap.ents.get(n).getClass() == Building.class){
					for(int v = 0; v < EntityMap.ents.get(n).villagers.size(); v++){
						if(x == EntityMap.ents.get(n).villagers.get(v).xco && y == EntityMap.ents.get(n).villagers.get(v).yco){
							go = false;
							if(x == gx && y == gy){
								go = true;
							}
						}
					}
				}
			}
		}
		if(go){
			for(int n = 0; n < open.size(); n++){
				if(open.get(n).x == x && open.get(n).y == y){
					if(closed.get(ci).g + 1 < open.get(n).g){
						open.get(n).g = closed.get(ci).g + 1;
						open.get(n).parent = closed.get(ci);
						open.get(n).f = open.get(n).g + open.get(n).h;
						go = false;
					}
				}
			}
		}
		if(go){
			open.add(new Node(x, y, gx, gy, closed.get(ci)));
		}
	}
	
	public static ArrayList<Integer> result(){
		ArrayList<Integer> result = new ArrayList<Integer>();
		double x;
		double y;
		double x1;
		double y1;
		boolean made = false;
		int current = closed.size() - 1;	
		while(!made){
			x = closed.get(current).x;
			y = closed.get(current).y;
			x1 = closed.get(current).parent.x;
			y1 = closed.get(current).parent.y;
			if(x > x1){
				result.add(0, 1);
			}
			else if(x < x1){
				result.add(0, 3);
			}
			else if(y > y1){
				result.add(0, 2);
			}
			else if(y < y1){
				result.add(0, 4);
			}
			result.add(0, 0);
			boolean found = false;
			for(int n = 0;!found && n < closed.size(); n++){
				if(closed.get(current).parent.x == closed.get(n).x && closed.get(current).parent.y == closed.get(n).y){
					current = n;
					found = true;
				}
			}
			if(closed.get(current).parent == null){
				made = true;
			}
		}
		
		
		return result;
	}
}