package com.ion.ld25;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import com.ion.ld25.entities.Bandit;
import com.ion.ld25.gfx.DrawString;
import com.ion.ld25.gfx.Graph;
import com.ion.ld25.gfx.ImageHandler;
import com.ion.ld25.map.EntityMap;
import com.ion.ld25.map.Map;

public class Game extends Applet implements Runnable, KeyListener{
	
	private static final long serialVersionUID = 1L;
	
	private Image dbImage;
	private Graphics dbg;
	
	public static int mapSize = 500;
	public static int width = 750;
	public static int height = 600;
	
	int time = 0;
	
	
	
	public boolean titleScreen = true;
	public boolean titleScreenEnabled = true;
	
	public static BufferedImage tileSheet;
	public static BufferedImage letters;
	public static BufferedImage title;
	public static BufferedImage play;
	public static BufferedImage[] tiles;
	public static BufferedImage[] letter;
	
	public static BufferedImage[] bandits;
	public static BufferedImage[] villagers;
	public static BufferedImage[] buildings;
	public static BufferedImage[] goats;
	
	public static BufferedImage focus;
	
	public static int active = - 1;
	
	public void init(){
		setSize(width, height);
		addKeyListener(this);
		setFocusable(true);
		setBackground(Color.black);
		try {
			tileSheet = ImageIO.read(Game.class.getClassLoader().getResourceAsStream("res/tiles.png"));
			title = ImageIO.read(Game.class.getClassLoader().getResourceAsStream("res/title.png"));
			letters = ImageIO.read(Game.class.getClassLoader().getResourceAsStream("res/letters.png"));
			Graph.popups = ImageIO.read(Game.class.getClassLoader().getResourceAsStream("res/focus.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		tiles = ImageHandler.all(tileSheet, 9, 11, 0xff00ff, 0x00FFFFFF, 2);
		
		letter = ImageHandler.all(letters, 27, 2, 0xff00ff, 0x00FFFFFF, 1);
		
		
		bandits = Arrays.copyOfRange(tiles, 18, 35);
		villagers = Arrays.copyOfRange(tiles, 36, 90);
		buildings = Arrays.copyOfRange(tiles, 6, 17);
		goats = Arrays.copyOfRange(tiles, 90, 96);
		
		focus = DrawString.make("CLICK TO FOCUS", 14, 2, 0x40C040);
		play = DrawString.make("( PLAY )", 8, 4, 0x1500FF);
		
	}
	
	public void newGame(){
		//EntityMap.clear();
		
		Map.generateMap();
		
		EntityMap.genClan(8);
		EntityMap.genVillages(5, 4, 3, 1);
		EntityMap.genTree(20000);
		
		EntityMap.genGoats(1);
		
		
		if(!titleScreenEnabled) titleScreen = false;
	}
	
	public void start(){
		Thread th = new Thread(this);
		th.start();
	}
	
	public void run(){
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		while(true){
			repaint();
			time++;
			if(time >= 25){
				if(Graph.a == 0) Graph.a = 9;
				else if(Graph.a == 9) Graph.a = 0;
				time = 0; 
			}
			
			
			try{
				Thread.sleep(20);
			} 
			catch(InterruptedException ex){
				ex.printStackTrace();
			}
			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		}
	}
	
	public void paint(Graphics g){
		if(titleScreen){
			g.drawImage(title, 0, 0, null);
			g.drawImage(play, width / 2 - play.getWidth() / 2, height / 2 - play.getHeight() / 2, null);
		}
		else{
			Graph.updateGraphics(g);
			if(!this.hasFocus()){
				Graph.focusPopUp(g);
			}
		}
	}
	
	public void update (Graphics g){
		if (dbImage == null){
	        dbImage = createImage (this.getSize().width, this.getSize().height);
	        dbg = dbImage.getGraphics ();
	    }
	    dbg.setColor (getBackground ());
	    dbg.fillRect (0, 0, this.getSize().width, this.getSize().height);
	    dbg.setColor (getForeground());	    
	    paint (dbg);
	    g.drawImage (dbImage, 0, 0, this);
	}

	public void keyPressed(KeyEvent e) {
		if(!titleScreen){
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Graph.up = true;
			}
			if(e.getKeyCode() == KeyEvent.VK_DOWN){
				Graph.down = true;
			}
			if(e.getKeyCode() == KeyEvent.VK_LEFT){
				Graph.left = true;
			}
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				Graph.right = true;
			}
			if(e.getKeyCode() == KeyEvent.VK_W && Graph.xcoc > 0){
				Graph.xcoc--;
			}
			if(e.getKeyCode() == KeyEvent.VK_S && Graph.xcoc < Map.mapGround.length - 1){
				Graph.xcoc++;
			}
			if(e.getKeyCode() == KeyEvent.VK_D && Graph.ycoc > 0){
				Graph.ycoc--;
			}
			if(e.getKeyCode() == KeyEvent.VK_A && Graph.ycoc < Map.mapGround.length - 1){
				Graph.ycoc++;
			}
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				boolean a = false;
				for(int n = 0; n < EntityMap.ents.size(); n++){
					if(active == -1){
						if(EntityMap.ents.get(n).getClass() == Bandit.class){
							if(EntityMap.ents.get(n).xco == Graph.xcoc && EntityMap.ents.get(n).yco == Graph.ycoc){
								active = n;
								a = true;
							}
						}
					}
				}
				if(!a){
					if(active != -1){
						EntityMap.ents.get(active).dest = new Point(Graph.xcoc, Graph.ycoc);
					}
					active = -1;
				}
			}
		}
		else{
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				newGame();
				titleScreen = false;
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		if(!titleScreen){
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Graph.up = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_DOWN){
				Graph.down = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_LEFT){
				Graph.left = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				Graph.right = false;
			}
		}
	}

	public void keyTyped(KeyEvent e) {

	}
}
