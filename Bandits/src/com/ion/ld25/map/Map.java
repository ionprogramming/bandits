package com.ion.ld25.map;

import java.util.Random;

import com.ion.ld25.Game;

public class Map {

	public static int[][] mapGround = new int[Game.mapSize][Game.mapSize];	
	public static int[][] mapFog = new int[Game.mapSize][Game.mapSize];
	
	public static Random random = new Random();
	
	public static void generateMap(){

		for(int y = 0; y < Game.mapSize; y++){
			for(int x = 0; x < Game.mapSize; x++){
					mapFog[y][x] = 2;
			}
		}
		
		int passes = 12;
		
		for(int y = 0; y < Game.mapSize; y++){
			for(int x = 0; x < Game.mapSize; x++){
				int z = random.nextInt(60)+1;
				if(z == 1){
					mapGround[y][x] = 0;
				}
				else{
					mapGround[y][x] = 1;
				}
			}
		}	
		
		for(int w = 0; w <= passes; w++){
			for(int y = 0; y < Game.mapSize; y++){
				for(int x = 0; x < Game.mapSize; x++){
					int chance = random.nextInt(100)+1;
					int dir = random.nextInt(4)+1;
					
					if(mapGround[y][x] == 0 && y >= 1 && x >=1 && y <= Game.mapSize-2 && x <= Game.mapSize-2){
						if(chance <= 50){
							
							if(dir == 1){
								mapGround[y-1][x] = 0;
							}
							else if(dir == 2){
								mapGround[y][x+1] = 0;
							}
							else if(dir == 3){
								mapGround[y+1][x] = 0;
							}
							else if(dir == 4){
								mapGround[y][x-1] = 0;
							}
						}
					}
					
				}
			}
	
		}
	}
}
