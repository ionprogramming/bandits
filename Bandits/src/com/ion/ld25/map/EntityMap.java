package com.ion.ld25.map;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import com.ion.ld25.Game;
import com.ion.ld25.entities.Bandit;
import com.ion.ld25.entities.Building;
import com.ion.ld25.entities.Entity;
import com.ion.ld25.entities.Goat;
import com.ion.ld25.entities.Tree;

public class EntityMap {
	public static ArrayList<Entity> ents = new ArrayList<Entity>();
	static Random random = new Random();
	public static ArrayList<Point> locations = new ArrayList<Point>();
	
	public static void clear(){
		ents.clear();
		Map.mapGround = null;
		Map.mapFog = null;
	}
	
	public static void genClan(int guys){
		ents.add(new Bandit(1, 1, Game.bandits));
		ents.add(new Bandit(1, 2, Game.bandits));
		ents.add(new Bandit(2, 1, Game.bandits));	
	}
	
	public static void genGoats(int num){
		for(int n = 0; n < num; n++){
			ents.add(new Goat(random.nextInt(5), random.nextInt(5), Game.goats));	
		}	
	}
	
	
	public static void genVillages(int sml, int med, int lrg, int xlrg){
		locations.clear();
		for(int n = 0; n < sml + med + lrg + xlrg; n++){
			int timeout = 5000;
			boolean done = false;
			int x = 0;
			int y = 0;
			while(!done){
				done = true;
				x = random.nextInt(Game.mapSize - 40) + 20;
				y = random.nextInt(Game.mapSize - 40) + 20;
				for(int v = 0; v < locations.size(); v++){
					if(Math.abs(x - locations.get(v).x) < 100 && Math.abs(y - locations.get(v).y) < 100){
						done = false;
					}
				}
				timeout--;
				if(timeout == 0){
					done = true;
				}
			}
			locations.add(new Point(x, y));
			if(timeout == 0){
				locations.clear();
				n = 0;
			}
		}
		int current;
		ArrayList<Point> plots = new ArrayList<Point>();
		for(int n = 0; n < sml; n++){
			current = random.nextInt(locations.size());
			for(int s = 0; s < 4; s++){
				plots = getNum(locations.get(current), 5, plots);
			}
			locations.remove(current);
			ents.add(new Building(plots.get(0).x, plots.get(0).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(1).x, plots.get(1).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(2).x, plots.get(2).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(3).x, plots.get(3).y, Game.buildings, "greengrocer"));
			plots.clear();
		}
		for(int n = 0; n < med; n++){
			current = random.nextInt(locations.size());
			for(int s = 0; s < 7; s++){
				plots = getNum(locations.get(current), 7, plots);
			}
			locations.remove(current);
			ents.add(new Building(plots.get(0).x, plots.get(0).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(1).x, plots.get(1).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(2).x, plots.get(2).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(3).x, plots.get(3).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(4).x, plots.get(4).y, Game.buildings, "greengrocer"));
			ents.add(new Building(plots.get(5).x, plots.get(5).y, Game.buildings, "butcher"));
			ents.add(new Building(plots.get(6).x, plots.get(6).y, Game.buildings, "bank"));
			plots.clear();
		}
		for(int n = 0; n < lrg; n++){
			current = random.nextInt(locations.size());
			for(int s = 0; s < 11; s++){
				plots = getNum(locations.get(current), 9, plots);
			}
			locations.remove(current);
			ents.add(new Building(plots.get(0).x, plots.get(0).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(1).x, plots.get(1).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(2).x, plots.get(2).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(3).x, plots.get(3).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(4).x, plots.get(4).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(5).x, plots.get(5).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(6).x, plots.get(6).y, Game.buildings, "greengrocer"));
			ents.add(new Building(plots.get(7).x, plots.get(7).y, Game.buildings, "butcher"));
			ents.add(new Building(plots.get(8).x, plots.get(8).y, Game.buildings, "bank"));
			ents.add(new Building(plots.get(9).x, plots.get(9).y, Game.buildings, "inn"));
			ents.add(new Building(plots.get(10).x, plots.get(10).y, Game.buildings, "blacksmith"));
			plots.clear();
		}
		for(int n = 0; n < xlrg; n++){
			current = random.nextInt(locations.size());
			for(int s = 0; s < 14; s++){
				plots = getNum(locations.get(current), 11, plots);
			}
			locations.remove(current);
			ents.add(new Building(plots.get(0).x, plots.get(0).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(1).x, plots.get(1).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(2).x, plots.get(2).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(3).x, plots.get(3).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(4).x, plots.get(4).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(5).x, plots.get(5).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(6).x, plots.get(6).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(7).x, plots.get(7).y, Game.buildings, "house"));
			ents.add(new Building(plots.get(8).x, plots.get(8).y, Game.buildings, "greengrocer"));
			ents.add(new Building(plots.get(9).x, plots.get(9).y, Game.buildings, "butcher"));
			ents.add(new Building(plots.get(10).x, plots.get(10).y, Game.buildings, "bank"));
			ents.add(new Building(plots.get(11).x, plots.get(11).y, Game.buildings, "inn"));
			ents.add(new Building(plots.get(12).x, plots.get(12).y, Game.buildings, "blacksmith"));
			ents.add(new Building(plots.get(13).x, plots.get(13).y, Game.buildings, "blacksmith"));
			plots.clear();
		}
	}
	
	public static ArrayList<Point> getNum(Point center, int range, ArrayList<Point> taken){
		boolean done = false;
		int x = 0;
		int y = 0;
		while(!done){
			done = true;
			x = random.nextInt(range);
			y = random.nextInt(range);
			if(random.nextBoolean()){
				x = center.x + x;
			}
			else{
				x = center.x - x;
			}
			if(random.nextBoolean()){
				y = center.y + y;
			}
			else{
				y = center.y - y;
			}
			Point p = new Point(x, y);
			for(int n = 0; n < taken.size(); n++){
				if(p.distance(taken.get(n)) < 3){
					done = false;
				}
			}
		}
		taken.add(new Point(x, y));
		return taken;
	}
	
	public static void genTree(int trees){
		for(int n = 0; n < trees; n++){
			boolean done = false;
			int x = 0;
			int y = 0;
			while(!done){
				done = true;
				x = random.nextInt(Game.mapSize);
				y = random.nextInt(Game.mapSize);
				for(int e = 0; e < ents.size(); e++){
					if(ents.get(e).xco == x && ents.get(e).yco == y){
						done = false;
					}
					if(ents.get(e).getClass() == Building.class){
						for(int v = 0; v < ents.get(e).villagers.size(); v++){
							if(ents.get(e).villagers.get(v).xco == x && ents.get(e).villagers.get(v).yco == y){
								done = false;
							}
						}
					}
				}
			}
			ents.add(new Tree(x, y, Game.tiles[3]));
		}
	}
	
	
}
