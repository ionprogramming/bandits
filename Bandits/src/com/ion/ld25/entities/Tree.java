package com.ion.ld25.entities;

import java.awt.image.BufferedImage;

import com.ion.ld25.map.Map;

public class Tree extends Entity{
	public Tree(int x, int y, BufferedImage image){		
		xco = x;
		yco = y;
		sum = xco + yco;
		healthRange = 10;
		health = Map.random.nextInt(10)+1;
		currentImage = image;
		type = "tree";
	}
}
