package com.ion.ld25.entities;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import com.ion.ld25.gfx.Graph;
import com.ion.ld25.map.EntityMap;
import com.ion.ld25.map.Map;

public class Entity {
	public String type = "";
	public int xco;
	public int yco;
	public int xpos;
	public int ypos;
	public int xh;
	public int yh;
	public int sum;
	public int health;
	public int healthRange = 100;
	public Point dest = null;
	public int dir;
	public int timer;
	public ArrayList<Entity> villagers;
	public ArrayList<Integer> path = new ArrayList<Integer>();
	public Point oldDest = null;
	BufferedImage[] images;
	BufferedImage currentImage;
	Random random = new Random();
	
	public void update(){
		ai();
		xpos = (xco - yco)*32;
		ypos = (xco + yco)*20;
	}
	
	public void draw(Graphics g){
		g.drawImage(currentImage, xpos + Graph.scrx, ypos + Graph.scry, null);
	}
	
	public void ai(){
	}
	
	public void move(int d){
		int targetX = xco;
		int targetY = yco;
		boolean go = true;
		if(d == 1){
			targetX = xco + 1;
		}
		else if(d == 2){
			targetY = yco + 1;
		}
		else if(d == 3){
			targetX = xco - 1;
		}
		else if(d == 4){
			targetY = yco - 1;
		}
		for(int n = 0; n < EntityMap.ents.size(); n++){
			if(EntityMap.ents.get(n).xco == targetX && EntityMap.ents.get(n).yco == targetY){
				go = false;
			}
			if(EntityMap.ents.get(n).getClass() == Building.class){
				for(int v = 0; v < EntityMap.ents.get(n).villagers.size(); v++){
					if(EntityMap.ents.get(n).villagers.get(v).xco == targetX && EntityMap.ents.get(n).villagers.get(v).yco == targetY){
						go = false;
					}
				}
			}
		}
		if(go){
			if(d == 1 && xco < Map.mapGround.length - 1){
				xco++;
				timer = 10;
				dir = d;
			}
			else if(d == 2 && yco < Map.mapGround.length - 1){
				yco++;		
				timer = 10;
				dir = d;
			}
			else if(d == 3 && xco > 0){
				xco--;
				timer = 10;
				dir = d;
			}
			else if(d == 4 && yco > 0){
				yco--;
				timer = 10;
				dir = d;
			}
		}
	}
}
