package com.ion.ld25.entities;

import java.awt.image.BufferedImage;
import java.util.Arrays;


public class Villager extends Entity{ 
	
	public Villager(int x, int y, BufferedImage[] imgs, String typeVillager){
		xco = x;
		yco = y;
		healthRange = 100;
		health = 100;
		images = imgs;	
		type = typeVillager;
	}
	
	public void ai(){
		if(timer == 0){
			if(random.nextInt(50) == 0){
				int xdest = xco;
				int ydest = yco;
				int d  = random.nextInt(4) + 1;
				if(d == 1){
					xdest += 1;
				}
				else if(d == 2){
					ydest += 1;
				}
				else if(d == 3){
					xdest -= 1;
				}
				else if(d == 4){
					ydest -= 1;
				}
				if(Math.abs(xdest - xh) < 10 && Math.abs(ydest - yh) < 10 && type != "farmer"){
					move(d);
				}
				if(Math.abs(xdest - xh) < 20 && Math.abs(ydest - yh) < 20 && type == "farmer"){
					move(d);
				}
			}
		}
		sum = xco + yco;
		if(type == "farmer"){
			images(Arrays.copyOfRange(images, 0, 8));
		}
		else if(type == "banker"){
			images(Arrays.copyOfRange(images, 9, 17));
		}
		else if(type == "butcher"){
			images(Arrays.copyOfRange(images, 18, 26));
		}
		else if(type == "blacksmith"){
			images(Arrays.copyOfRange(images, 27, 35));
		}
		else if(type == "innkeeper"){
			images(Arrays.copyOfRange(images, 36, 44));
		}
		else if(type == "grocer"){
			images(Arrays.copyOfRange(images, 45, 53));
		}
	}
	
	public void images(BufferedImage[] imgs){
		if(timer > 0){
			timer--;
			if(dir == 0){
				currentImage = imgs[0];
			}
			else if(dir == 1){
				currentImage = imgs[1];
			}
			else if(dir == 2){
				currentImage = imgs[2];
			}
			else if(dir == 3){
				currentImage = imgs[3];
			}
			else if(dir == 4){
				currentImage = imgs[4];
			}
		}
		else{
			dir = 0;
			currentImage = imgs[0];
		}
	}
}
