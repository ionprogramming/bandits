package com.ion.ld25.entities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.ion.ld25.Game;

public class Building extends Entity{
		
	public Building(int x, int y, BufferedImage[] imgs, String buildType){
		type = buildType;
		healthRange = 4;
		health = 4;
		villagers = new ArrayList<Entity>();
		xco = x;
		yco = y;
		sum = xco + yco;
		images = imgs;
		if(type == "house"){
			currentImage = images[0];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "farmer"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "farmer"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "farmer"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "farmer"));
		}
		else if(type == "bank"){
			currentImage = images[1];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "banker"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "banker"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "banker"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "banker"));
		}
		else if(type == "butcher"){
			currentImage = images[2];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "butcher"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "butcher"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "butcher"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "butcher"));
		}
		else if(type == "blacksmith"){
			currentImage = images[3];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "blacksmith"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "blacksmith"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "blacksmith"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "blacksmith"));
		}
		else if(type == "inn"){
			currentImage = images[4];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "innkeeper"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "innkeeper"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "innkeeper"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "innkeeper"));
		}
		else if(type == "greengrocer"){
			currentImage = images[5];
			villagers.add(new Villager(xco + 1, yco, Game.villagers, "grocer"));
			villagers.add(new Villager(xco - 1, yco, Game.villagers, "grocer"));
			villagers.add(new Villager(xco, yco + 1, Game.villagers, "grocer"));
			villagers.add(new Villager(xco, yco - 1, Game.villagers, "grocer"));
		}
		for(int n = 0; n < villagers.size(); n++){
			villagers.get(n).xh = xco;
			villagers.get(n).yh = yco;
		}
	}
			
	public void ai(){
		health = villagers.size();	
	}
	
	public void take(){
		if(villagers.size() == 0){
			if(type == "house"){
				currentImage = images[6];
			}
			else if(type == "bank"){
				currentImage = images[7];
			}
			else if(type == "butcher"){
				currentImage = images[8];
			}
			else if(type == "blacksmith"){
				currentImage = images[9];
			}
			else if(type == "inn"){
				currentImage = images[10];
			}
			else if(type == "greengrocer"){
				currentImage = images[11];
			}
		}
	}
		
}


