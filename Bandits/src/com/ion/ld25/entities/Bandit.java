package com.ion.ld25.entities;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import com.ion.ld25.astar.Astar;
import com.ion.ld25.map.Map;

public class Bandit extends Entity{
	
	public int rank = 1;
	public boolean attacking = false;
	
	public Bandit(int x, int y, BufferedImage[] imgs){
		type = "bandit";
		xco = x;
		yco = y;
		healthRange = 100;
		health = 100;
		images = imgs;
		dest = null;
		currentImage = imgs[0];
		dir = 0;
	}
		
	public void ai(){
		sum = xco + yco;
		if(rank == 1){
			images(Arrays.copyOfRange(images, 9, 17));
		}
		else if(rank == 2){
			images(Arrays.copyOfRange(images, 0, 8));
		}
		
		Map.mapFog[yco][xco] = 0;
		for(int x = 0; x < 3; x++){
			for(int y = 0; y < 3; y++){
				if(xco - x >= 0){
					if(yco - y >= 0){
						Map.mapFog[yco-y][xco-x] = 0;
					}
				}
				
				
				if(xco + x <= 499){
					if(yco + y <= 499){
						Map.mapFog[yco+y][xco+x] = 0;
					}
				}
				if(xco + x <= 499){
					if(yco - y >= 0){
						Map.mapFog[yco-y][xco+x] = 0;
					}
				}
				
				
				if(xco - x >= 0){
					if(yco + y <= 499){
						Map.mapFog[yco+y][xco-x] = 0;
					}
				}
				
				if(dest != null){
					path = Astar.path(xco, yco, dest.x, dest.y);
					oldDest = dest;
					dest = null;
				}
				if(timer == 0){
					if(path.size() != 0){
						move(path.get(0));
						path.remove(0);
					}
				}
				
						
			}
		}
		

		
		
	}
	
	public void images(BufferedImage[] imgs){
		if(timer > 0){
			timer--;
			if(dir == 0){
				currentImage = imgs[0];
			}
			else if(dir == 1){
				currentImage = imgs[1];
			}
			else if(dir == 2){
				currentImage = imgs[2];
			}
			else if(dir == 3){
				currentImage = imgs[3];
			}
			else if(dir == 4){
				currentImage = imgs[4];
			}
		}
		else{
			dir = 0;
			currentImage = imgs[0];
		}
	}
}                        

