package com.ion.ld25.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.ion.ld25.Game;
import com.ion.ld25.entities.Building;
import com.ion.ld25.entities.Entity;
import com.ion.ld25.map.EntityMap;
import com.ion.ld25.map.Map;

public class Graph {
	
	public static boolean up = false;
	public static boolean down = false;
	public static boolean left = false;
	public static boolean right = false;
	
	public static int scrx = 0;
	public static int scry = 0;
	public static int a = 0;
	public static BufferedImage popups;
	
	public static ArrayList<Entity> paintEnts = new ArrayList<Entity>();
	public static ArrayList<Entity> ordered = new ArrayList<Entity>();
	
	public static int xcoc = 0;
	public static int ycoc = 0;
	
	
	public static void updateGraphics(Graphics g){
		
		if(up && xcoc > 0){
			xcoc--;
		}
		if(down && xcoc < Map.mapGround.length - 1){
			xcoc++;
		}
		if(right && ycoc > 0){
			ycoc--;
		}
		if(left && ycoc < Map.mapGround.length - 1){
			ycoc++;
		}
		scrx = -(xcoc*32 - ycoc*32) + Game.width/2 - Game.tiles[5].getWidth() / 2;
		scry = -(xcoc*20 + ycoc*20) + Game.height/2 - Game.tiles[5].getHeight() / 2;
		drawMap(g);
	}
	
	public static void focusPopUp(Graphics g){
		BufferedImage newsize;
		BufferedImage[] font;

		newsize = ImageHandler.resizeImage(popups, popups.getWidth()*8, popups.getHeight()*8); 
		font = ImageHandler.splitImage(newsize, 9, 2, 8);
		int msgw = 512;
		int msgh = 256;
	
		g.drawImage(font[0 + a], (Game.width / 2) - (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) - (msgh / 2) - font[0].getWidth()/2, null);
		g.drawImage(font[1 + a], (Game.width / 2) + (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) - (msgh / 2) - font[0].getWidth()/2, null);
		g.drawImage(font[3 + a], (Game.width / 2) - (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) + (msgh / 2) - font[0].getWidth()/2, null);
		g.drawImage(font[2 + a], (Game.width / 2) + (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) + (msgh / 2) - font[0].getWidth()/2, null);
		for(int x = 64; x < 64 *4; x +=64){
			g.drawImage(font[4 + a], (Game.width / 2) - (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) - (msgh / 2) - font[0].getWidth()/2+ x, null);
		}
		for(int y = 64; y < 64 *8; y +=64){
			g.drawImage(font[5 + a], (Game.width / 2) - (msgw / 2) - font[0].getWidth()/2 + y, (Game.height / 2) - (msgh / 2) - font[0].getWidth()/2, null);
		}
		for(int x = 64; x < 64 *4; x +=64){
			g.drawImage(font[6 + a], (Game.width / 2) + (msgw / 2) - font[0].getWidth()/2, (Game.height / 2) - (msgh / 2) - font[0].getWidth()/2 + x, null);
		}
		for(int y = 64; y < 64 *8; y +=64){
			g.drawImage(font[7 + a], (Game.width / 2) - (msgw / 2) - font[0].getWidth()/2 + y, (Game.height / 2) + (msgh / 2) - font[0].getWidth()/2, null);
		}
		g.setColor(new Color(73,68,65));
		g.fillRect(Game.width/2 - msgw/2 + font[0].getWidth()/2, Game.height/2 - msgh/2 + font[0].getWidth()/2, 64*7, 64*3);
		g.drawImage(Game.focus, Game.width / 2 - Game.focus.getWidth() / 2, Game.height / 2 - Game.focus.getHeight() / 2, null);
		
	}
	
	public static void drawMap(Graphics g){
		for(int y = 0; y < Map.mapGround.length; y++){
			int xpos = -y*32;
			int ypos = y*20;
			for(int x = 0; x < Map.mapGround[0].length; x++){
				if(xpos + scrx >= -64 && ypos + scry >= -64 && xpos + scrx <= Game.width && ypos + scry <= Game.height){
					g.drawImage(Game.tiles[Map.mapGround[y][x]], xpos + scrx, ypos + scry, null);
				}
				xpos += 32;
				ypos += 20;
			}
		}
		
		if(Game.active == -1){
			g.drawImage(Game.tiles[5], Game.width/2 - Game.tiles[5].getWidth() / 2, Game.height/2 - Game.tiles[5].getHeight() / 2, null);
		}
		else{
			g.drawImage(Game.tiles[4], Game.width/2 - Game.tiles[5].getWidth() / 2, Game.height/2 - Game.tiles[5].getHeight() / 2, null);
		}
		
	
		for(int n = 0; n < EntityMap.ents.size(); n++){
			EntityMap.ents.get(n).update();
			
			if(EntityMap.ents.get(n).getClass() == Building.class){
				for(int v = 0; v < EntityMap.ents.get(n).villagers.size(); v++){
					EntityMap.ents.get(n).villagers.get(v).update();
					if(EntityMap.ents.get(n).villagers.get(v).xpos + Graph.scrx >= -64 && EntityMap.ents.get(n).villagers.get(v).ypos + Graph.scry >= -64 && EntityMap.ents.get(n).villagers.get(v).xpos + Graph.scrx <= Game.width && EntityMap.ents.get(n).villagers.get(v).ypos + Graph.scry <= Game.height){
						paintEnts.add(EntityMap.ents.get(n).villagers.get(v));
					}
				}
			}
			if(EntityMap.ents.get(n).xpos + Graph.scrx >= -64 && EntityMap.ents.get(n).ypos + Graph.scry >= -64 && EntityMap.ents.get(n).xpos + Graph.scrx <= Game.width && EntityMap.ents.get(n).ypos + Graph.scry <= Game.height){
				paintEnts.add(EntityMap.ents.get(n));
			}
		}
		
		if(paintEnts.size() >= 1){
			int max = 0;
			int index = 0;
			for(int n = 0; n < paintEnts.size(); n++){
				if(paintEnts.get(n).sum > max){
					max = paintEnts.get(n).sum;
					index = n;
				}
			}
			ordered.add(paintEnts.get(index));
			paintEnts.remove(index);
			
			for(int n = 0; n < paintEnts.size(); n++){
				for(int i = 0; i < ordered.size(); i++){
					if(paintEnts.get(n).sum <= ordered.get(i).sum){
						ordered.add(i, paintEnts.get(n));
						break;
					}
				}
			}
			
			for(int n = 0; n < ordered.size(); n++){
				ordered.get(n).draw(g);
			}
			
			for(int y = 0; y < Map.mapFog.length; y++){
				int xpos = -y*32;
				int ypos = y*20;
				for(int x = 0; x < Map.mapFog[0].length; x++){
					if(xpos + scrx >= -64 && ypos + scry >= -64 && xpos + scrx <= Game.width && ypos + scry <= Game.height && Map.mapFog[y][x] != 0){
						//g.drawImage(Game.tiles[Map.mapFog[y][x]], xpos + scrx, ypos + scry, null);
					}	
					xpos += 32;
					ypos += 20;
				}
			}
		HUD.draw(g);
			ordered.clear();
			paintEnts.clear();
		}
		
		
		
		
		
	}
}
