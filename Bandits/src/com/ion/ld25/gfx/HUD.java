package com.ion.ld25.gfx;

import java.awt.Color;
import java.awt.Graphics;

import com.ion.ld25.Game;
import com.ion.ld25.entities.Entity;

public class HUD {
	static Entity currentEnt;
	static int height = 40;
	public static void draw(Graphics g){
		g.setColor(Color.BLACK);;
		g.fillRect(0, Game.height - height, Game.width, height);
		g.drawImage(DrawString.make((Graph.xcoc +1)+ "," + (Graph.ycoc +1), 10, 1, 0xffffff), 500, Game.height - (height / 2) - 10, null);
		for(int n = 0; n < Graph.paintEnts.size(); n++){
			if(Graph.paintEnts.get(n).xco == Graph.xcoc && Graph.paintEnts.get(n).yco == Graph.ycoc){
				Entity currentEnt = Graph.paintEnts.get(n);
				g.drawImage(DrawString.make(currentEnt.type, 13, 1, 0xffffff), 20, Game.height - (height / 2) - 10, null);
				drawHealth(200, Game.height - (height / 2) - 10, 15, 100, currentEnt.healthRange, currentEnt.health, g, Color.RED);
			}
		}			
	}
	
	public static void drawHealth(int x, int y, int height, int width,int range, int health, Graphics graphics, Color outlineCol){
		float g = (width / range * health) * 2.55f;
		float r = 255 - g;
		Color col = new Color((int)r,(int)g, 0);
		graphics.setColor(outlineCol);
		graphics.drawRect(x-1, y-1, width+1, height+1);
		graphics.setColor(col);
		graphics.fillRect(x, y, width / range * health, height);
		
	}

}
